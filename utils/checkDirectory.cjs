const fs = require('fs');
const path = require('path');

function checkDirectory(directoryName) {

    return new Promise((resolve, reject) => {

        if (directoryName.length === 0) {
            reject([400, "directory name is empty"]);
        } else if (directoryName.includes("/")) {
            reject([400, "cannot include / in directory name"]);
        } else {
            const directoryPath = path.join(__dirname, "../public", directoryName);

            fs.access(directoryPath, (err) => {
                if (err) {
                    console.error(err);
                    reject([404, 'no directory found, try to change the name']);
                } else {
                    resolve();
                }
            });
        }
    });
}

module.exports = checkDirectory;